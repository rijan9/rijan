class MyClass:
  x = 5

print(MyClass)

p1 = MyClass()
print(p1.x)
class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

p1 = Person("Raam", 16)

print(p1.name)
print(p1.age)

class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

  def myfunc(self):
    print("Hello my name is " + self.name)

p1 = Person("Raam", 16)
p1.myfunc()