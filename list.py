list = ["apple", "banana", "cherry"] #lists are used to store multiple items in a single variable.
print(list)

list = ["apple", "banana", "cherry", "apple", "cherry"] #Lists allow duplicate values
print(list)

thislist = ["apple", "banana", "cherry"] #Print 1st item of the list
print(thislist[0])

thislist = ["apple", "banana", "cherry"]
print(thislist[-1]) #Print last item of the list

thislist = ["apple", "banana", "cherry"]
thislist[1] = "kwiks" # chnage the 2nd item of the list
print(thislist)